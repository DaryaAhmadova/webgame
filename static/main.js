var def = {
    cell_width: 31,
    cell_height: 31,
    cell_row_count: 30,
    cell_col_count: 20,
    field_shit_x: 0,
    field_shit_y: 0,
    field: new Array(),
    socket: {},
    player: {},
    captured_resource_cells: []
};



function init()
{   
    def.player.money_increase_speed = 0;
    def.player.position = {'x': 0, 'y': 0};
    def.player.updatePosition = function(){
        $('.field .base').removeClass();
        $('#'+this.position.x+'_'+this.position.y).removeClass();
        $('#'+this.position.x+'_'+this.position.y).addClass('base');
    };
    
    def.player.updateMoney = function(){
        var money = (def.player.money+"$");
         $('.info .money').html(money);
    }
    
    def.player.updateMoneySpeed = function(){
         var money_speed = def.player.money_increase_speed;
         $('.info .speed').html(money_speed);
    }
    
    def.player.cells = [];
    def.player.updateCells = function(){
        //$('.field .captured').removeClass('captured');
        for (var cell in this.cells){
            var important_cells = $('#'+this.cells[cell].x+"_"+this.cells[cell].y);
            important_cells.addClass('captured');
            important_cells.addClass('player'+def.player.color_number);
        }
    };
    def.player.updateColors = function(){
	$('.field .base').addClass('player'+def.player.color_number)
        
    };
    def.player.base = {'x': 0, 'y': 0};
    def.player.money = 1000;
    
    def.player.bindData = function(data){
        if (typeof data['position'] !== 'undefined'){
            this.position = data['position'];
            this.updatePosition();
        }
	if (typeof data['color_number'] !== 'undefined'){
	    this.color_number = data['color_number'];
	    this.updateColors();
	}
        if (typeof data['cells'] !== 'undefined'){
            this.cells = data['cells'];
            this.updateCells();
            this.updateColors();
        }
        if (typeof data['base'] !== 'undefined')
            this.base = data['base'];
        if (typeof data['money'] !== 'undefined'){
            this.money = data['money'];
            this.updateMoney();
        }
        if (typeof data['money_increase_speed'] !== 'undefined'){
            this.money_increase_speed = data['money_increase_speed'];
            this.updateMoneySpeed()
        }
        
        
        
    };
}

function update_captured_cells(cells, player){
    for(cell in cells){
        var x = cells[cell]['x'];
        var y = cells[cell]['y'];
        $cell = $("#"+x+"_"+y);
        $cell.addClass('captured');
        $cell.addClass('player'+player);
    }
}

function updateUsersBlock()
{
    new_html = "";
    for(user in def.users){
        style = ""
        if (!def.users[user]['active'])
            style = "text-decoration: line-through;"
        new_html += '<p style="'+style+'">'+def.users[user]['email']+'</p>';
    }
    $('.users .content').html(new_html);
}

function getCellInstance(x, y, cell){
    new_cell = $(cell.clone());
    new_cell.click(
            function (){
                send_command("capture_cell", {'x':x, 'y':y});
            }
    );
    new_cell.dblclick(
            function (){
                send_command("kill", {'x':x, 'y':y});
            }
    );
    new_cell.css("left", x*def.cell_width+def.field_shit_x);
    new_cell.css("top", y*def.cell_height+def.field_shit_y);
    new_cell.attr("id", x+"_"+y);
    new_cell.data({'x': x, 'y': y});

    new_cell.addClass(def.field[y][x]);
    return new_cell;
}

function initField()
{
    $('.ready').click(
            function(){
                send_command("ready", []);
            }
     );
     
    $('.bot').click(
            function(){
                send_command("add_bot", []);
            }
     );
    
    for(var y = 0; y < def.cell_col_count; y++){
        def.field[y] = new Array();
        for(var x = 0; x<def.cell_row_count; x++){
                def.field[y][x] = 'resource';
        }
    }
    
       
   cell = $('.field div');
   cell.css("position", "absolute");
   field = $('.field');
   
   $('.field div').remove();
   for (var y = 0; y < def.cell_col_count; y++)
       for (var x = 0; x < def.cell_row_count; x++){
           field.append(getCellInstance(x, y, cell));
       }
}

function updateField(field)
{
    for(cell in field){
        var x = field[cell]['x'];
        var y = field[cell]['y'];
        
        if (field[cell]['type'] === "resource" && typeof field[cell]['player'] !== 'undefined'){
            tcell = $("#"+x+"_"+y);
            tcell.addClass('resource');
            tcell.addClass('captured');
            tcell.addClass('player'+field[cell]['player']);
        } 
        else
            changeCellType(x, y, field[cell]['type']);
    }

}

function changeCellType(x, y, type){
    def.field[y][x] = type;
    $cell = $("#"+x+"_"+y);
    $cell.removeClass();
    $cell.addClass(type);
}

$(document).ready(function() {
   init();
   initField();
   openConnection();
});



function check_message(e){
    //console.log(e);
    var data = JSON.parse(e.data);
    console.log(data['command']);
    console.log(data['data']);
    switch(data['command']){
        case 'load_field':
            updateField(data['data']);
            break;
        case 'load_player':
            def.player.bindData(data['data']);
            break;
        case 'update_money':
            def.player.money = data['data']['money'];
            updatePlayer();
            break;
        case 'change_cell':
            var cell = data['data']['data'];
            break;
        case 'load_users':
            def.users = data['data']['users'];
            updateUsersBlock();
            break;
        case 'update_captured_cells':
            var captured_cells = data['data']['cells'];
            for (player in captured_cells){
                update_captured_cells(captured_cells[player], player)
            }
            break;
        case "winner":
            alert(data['data']+" won the game!");
            break;
    }
}

function own_cell(x, y){
    
}

function send_command(command, data)
{
    def.socket.send(
                JSON.stringify(
                    {
                        'command' : command,
                        'data': data
                    }
                )
            );
}


function openConnection(){
    def.socket = new WebSocket("ws://192.168.1.3:80/chat");
    def.socket.onopen = function (){
        send_command('authenticate', getCookie('user'))
        send_command('load_field', '');
        send_command('load_player', '');
        send_command('load_users', '');
        console.log("connection was opened");
    };

    def.socket.onmessage = check_message;

    def.socket.onerror = function(error) {
        //alert("Ошибка " + error.message);
        console.log("error occured");
        console.log(error)
    };
    
    def.socket.onclose = function(){
        console.log("connection was closed")
    }
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

